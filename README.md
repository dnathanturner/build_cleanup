# Build Cleanup
Deletes all jenkins builds older than 7 days.

## Pre-requisites

requires docker

## Setup

1. clone repository into {JENKINS HOME}
2. cd into repo
3. build using docker

	`docker run --rm -v $PWD:/cleaner -w /cleaner golang go build`
	
4. move binary into the {JENKINS HOME}/jobs directory
5. run as {JENKINS HOME} owner or with sudo
