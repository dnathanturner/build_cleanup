package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"time"
)

func main() {
	now := time.Now()
	seven_days_ago := now.Add(-time.Hour * 24 * 7)
	regex, _ := regexp.Compile("[0-9]")
	dirs, err := ioutil.ReadDir(".")
	if err != nil {
		log.Fatal(err)
	}

	for _, dir := range dirs {
		if dir.IsDir() {
			fmt.Printf("Entering: %s\n", dir.Name())

			subdirs, err := ioutil.ReadDir(dir.Name())
			if err != nil {
				log.Fatal(err)
			}

			for _, subdir := range subdirs {
				if subdir.Name() == "builds" {
					fmt.Printf("\tEntering: %s\n", subdir.Name())

					builds, err := ioutil.ReadDir(dir.Name() + "/" + subdir.Name())
					if err != nil {
						log.Fatal(err)
					}

					for _, build := range builds {
						if regex.MatchString(build.Name()) && build.ModTime().Before(seven_days_ago) {
							fmt.Printf("\t\tRemoving: %s. Last mod: %s\n", build.Name(), build.ModTime())
							os.RemoveAll(dir.Name() + "/" + subdir.Name() + "/" + build.Name())
						}
					}
				}
			}
		}
	}
}
